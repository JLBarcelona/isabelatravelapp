<!DOCTYPE html>
<html>
<title>Main Page</title>
  <?php include("./Layout/header.php") ?>
  <!-- Header css meta -->
<body class="sidebar-mini layout-fixed ">
  <div class="wrapper">
  <!-- navbar -->
  <?php include("./Layout/nav.php") ?>
  <!-- Sidebar -->
  <?php include("./Layout/sidebar.php") ?>
   <section class="content-wrapper bg-white">
      <div class="container-fluid">
        <div class="row pt-3">
         <div class="col-sm-12 text-center mb-3 h4 title">
            <span class="bold"><?php echo $_REQUEST['city_name'] ?></span> <br> <small>Categories</small>
          </div>
        </div>
        <div class="row menus_filter_category">
        </div>
      </div>
    </section>
  </div>
</body>
<!-- sorting_disabled -->
  <!-- Footer Scripts -->
  <?php include("./Layout/footer.php") ?>

  <script type="text/javascript">
    show_menus();
    show_category('<?php echo $_REQUEST['city'] ?>','<?php echo $_REQUEST['city_name'] ?>');
  </script>
  <!-- <button onclick="locator('16.6830107,121.528243', '16.68569274633472,121.53985977172853');">Hey</button> -->

  <!-- Map 3rd party -->
  <!-- https://www.google.com/maps/dir/16.7197503,121.5678336/16.6733797,121.5530677/@16.6854656,121.552896,13z -->

