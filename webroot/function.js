//all that function provides is made by JLDESIGNS FrameWorks*******
let url ='function.php';
//load_pages 


function tool(){
$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip(); 
});
}



function num_only(evt){
	var charcode= (evt.which) ? evt.which : event.keycode
	if (charcode > 31 && (charcode < 48 ||  charcode >57)){
		alert("number only");
		return false;
	}
	else
	{
		return true;
	}	
}


function login_user(){
var username = $("#username");
var password = $("#password");
if (username.val() == "") 
{
username.focus();
alerts("Enter your username !","alert_log","bg-danger");
}
else if (password.val() == "") 
{
password.focus();
alerts("Enter your password !","alert_log","bg-danger");
}
else
{
var mydata = 'action=login' + '&uname=' + username.val() + '&pwd=' + password.val();
$.ajax({
	type:"POST",
	url:url,
	data:mydata,
	cache:false,
	beforeSend:function(){
		loaders('logging');
	},
	success:function(data){
	// alert(data);
	setTimeout(function(){
		$("#logging").html('');
		if (data == 1) 
		{
			// swal("Admin","Admin","success");
			window.location="admin/";
		}
		else
		{
			swal("Sorry","Invalid Account!","error");

			// alert(data);
		}
	},500);
	}
});
}
}

function alert_info(msg,id){
$("#"+id).html('<span class="alert bg-danger text-light mr-2">'+msg+'</span>');
}

function alerts(msg,id,type){
$("#"+id).html('<span class="alert '+type+' text-light mr-2">'+msg+'</span>');
}


function loaders(id){
	$("#"+id).html('<img src="assets/img/loading.gif" width="20" class="img-fluid" style="margin-top:-7px;" />');
}