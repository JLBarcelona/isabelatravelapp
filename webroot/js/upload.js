 var upload_count = 0;

  function show_upload(param){
    // Preview
    $("#input").val(param);
    $("#modal_upload").modal('show');
  }

  function previewFile() {
    const att = $("#input").val();
    const preview = $("#"+att+"_preview");
    const base64 = $("#"+att);
    const file = document.querySelector('input[type=file]').files[0];
    const reader = new FileReader();

    reader.addEventListener("load", function () {
      base64.val(reader.result);
      $("#preview_img").html('<img src="'+reader.result+'" alt="" class="img-fluid img-thumbnail animated fadeIn mb-2">');
      // $("#preview_img").attr('style','background-image: url(\''+reader.result+'\');');
    }, false);

    if (file) {
      reader.readAsDataURL(file);
    }
  }


  function upload(){
      const att = $("#input").val();
      const preview = $("#"+att+"_preview");
      const base64 = $("#"+att).val();
     // preview.html('<img src="'+base64+'" alt="" class="img-fluid img-thumbnail animated fadeIn mt-2">');
     // preview.attr('style','background-image: url(\''+base64+'\');');

    let tmp_ids = 'img_id_'+upload_count;
    let img = '';
    img +='<div class="col-sm-3 col-6 mb-1 mt-1" id="image_uploaded_'+tmp_ids+'">';
    img +='<textarea name="gallery_description['+upload_count+']" id="gallery_description_'+tmp_ids+'" cols="30" class="form-control" placeholder="Write Description"></textarea>';
    img += '<img src="../webroot/img/img.png" class="img-fluid img-bg img-thumbnail" style="background-image:url('+base64+')" alt="" width="225">';
    img +='<button class="btn btn-block btn-danger btn-sm" type="button" onclick="delete_image_list(\'image_uploaded_'+tmp_ids+'\')">Remove</button>';
    img +='<textarea name="gallery_photo['+upload_count+']" id="gallery_photo_'+tmp_ids+'" cols="30" class="hide">'+base64+'</textarea>';
    img +='</div>';
    preview.append(img);

     upload_count++;
     $("#modal_upload").modal('hide');
     $("#input").val('');
     $("#preview_img").html('');
     $("#"+att).val('');
  }

  function close(){
     $("#modal_upload").modal('hide');
     $("#input").val('');
       $("#preview_img").html('');
  }
 
 function delete_image_list(id){
  $("#"+id).slideUp('fast', () => {
    $("#"+id).remove();
  })
 }