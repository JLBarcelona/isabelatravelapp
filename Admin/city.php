<!DOCTYPE html>
<html>
<title>Municipality</title>
  <?php include("./Layout/header.php") ?>
  <!-- Header css meta -->
<body class="" onload="active_nav('city'); get_city()">
  <div class="wrapper">
  <!-- navbar -->
  <?php include("./Layout/nav.php") ?>
   <section class="content-page">
      <div class="container-fluid">
        <div class="row pt-3">
          <div class="col-sm-12">
              <div class="card">
              <div class="card-header bg-primary">
                <span class="h4"><i class="fa fa-map"></i> Municipality</span>
                <button class="btn btn-sm btn-dark float-right" onclick="add_municipality()"><i class="fa fa-plus"></i> Add Municipality</button>
              </div>
              <div class="card-body">
                 <table class="table table-bordered dt-responsive nowrap" id="tbl_city" style="width: 100%;"></table>
              </div>
              <div class="card-footer"></div>
            </div>
            
          </div>
        </div>
      </div>
    </section>
  </div>
</body>
 <div class="modal fade" role="dialog" id="municipal_form_modal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title modal-municipal">
            </div>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <form class="needs-validation" id="municipal_form" action="#" novalidate>
            <div class="form-row">
              <input type="hidden" id="municipality_id" name="municipality_id" placeholder="" class="form-control" required>
              <div class="form-group col-sm-12">
                 <div class="text-center">
                        <img src="../webroot/images/municipal.png" alt="User Image" width="153" id="avatar_preview">
                    </div>
                    <div class="text-center mt-2">
                      <button class="btn btn-success btn-sm" type="button" onclick="show_upload('avatar');">Add Icon</button>
                    </div>
                    <textarea class="hide" name="avatar" id="avatar"></textarea>
              </div>

              <div class="form-group col-sm-12">
                <label>Municipality Name </label>
                <input type="text" id="municipality_name" name="municipality_name" placeholder="Enter Municipality" class="form-control " required>
                <div class="invalid-feedback" id="err_municipality_name"></div>
              </div>

              <div class="form-group col-sm-12">
                <label>History</label>
                <textarea type="text" id="history" name="history" placeholder="Enter History" class="form-control " required></textarea>
                <div class="invalid-feedback" id="err_history"></div>
              </div>

              <div class="col-sm-12 text-right">
                <button class="btn btn-success btn-sm" type="submit">Save</button>
              </div>
            </div>
          </form>
          </div>
          <div class="modal-footer">
            
          </div>
        </div>
      </div>
    </div>


       <div class="modal fade" role="dialog" id="modal_upload">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <div class="modal-title">
                Upload
              </div>
              <a href="#" class="close" data-dismiss="modal">&times;</a>
            </div>
            <div class="modal-body">
              <input type="hidden" name="input" id="input">
              <div class="text-center">
                <div id="preview_img"></div>
              </div>
              <div class="custom-file">
                <input type="file" class="custom-file-input" onchange="previewFile();" accept="image/*" id="file" name="file" required>
                <label class="custom-file-label" for="validatedCustomFile">Choose file</label>
              </div>
            </div>
            <div class="modal-footer text-right">
              <button class="btn btn-default" onclick="close();" data-dismiss="modal">Close</button>
              <button class="btn btn-dark" onclick="upload();">Upload</button>
            </div>
          </div>
        </div>
      </div>
  <!-- Footer Scripts -->
  <?php include("./Layout/footer.php") ?>
  <script src="../webroot/js/upload2.js"></script>

</html>

<script type="text/javascript">
  function add_municipality(){
    $(".modal-municipal").text('Add Municipality');
    $("#municipal_form_modal").modal({'backdrop' : 'static'});
    $('#municipality_id').val('');
    $('#municipality_name').val('');
    $("#history").val('');
    $("#avatar").val('');
    $("#avatar_preview").attr('src', '../webroot/images/municipal.png');
  }
</script>


<!-- Javascript Function-->
<script>
  var tbl_city;
  function show_city(data){
    if (tbl_city) {
      tbl_city.destroy();
    }
    tbl_city = $('#tbl_city').DataTable({
    pageLength: 10,
    responsive: true,
    // ajax: url,
    data: data,
    deferRender: true,
    language: {
    "emptyTable": "No data available"
  },
    columns: [{
    className: '',
    "data": "municipality_name",
    "title": "Municipality Name",
  },{
    className: 'width-1 text-center',
    "data": "municipality_id",
    "orderable": false,
    "title": "Options",
      "render": function(data, type, row, meta){
        var param_data = JSON.stringify(row);
        newdata = '';
        newdata += '<button class="btn btn-success btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="edit_city(this)" type="button"><i class="fa fa-edit"></i></button>';
        newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" data-id=\' '+row.municipality_id+'\' onclick="delete_city(this)" type="button"><i class="fa fa-trash"></i></button>';
        return newdata;
      }
    }
  ]
  });
  }

  function get_city(){
    var url = url_user + '?action=show_city';
    $.ajax({
        type:"GET",
        url:url,
        data:{},
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
          show_city(response.data)
        },
        error: function(error){
          console.log(error);
        }
      });

  }

  $("#municipal_form").on('submit', function(e){
    // var url = $(this).attr('action');
    var url = url_user + '?action=add_city';
    var mydata = $(this).serialize();
    e.stopPropagation();
    e.preventDefault(e);

    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      dataType:'json',
      beforeSend:function(){
          //<!-- your before success function -->
      },
      success:function(response){
          //console.log(response)
        if(response.status == true){
          // console.log(response)
          swal("Success", response.message, "success");
          showValidator(response.error,'municipal_form');
          get_city();
          $("#municipal_form_modal").modal('hide');
        }else{
          //<!-- your error message or action here! -->
          showValidator(response.error,'municipal_form');
        }
      },
      error:function(error){
        console.log(error)
      }
    });
  });

  function delete_city(_this){
    var id = $(_this).attr('data-id');
    var url =  url_user + '?action=delete_city';
      swal({
        title: "Are you sure?",
        text: "Do you want to delete this city?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnConfirm: false
      },
      function(){
        $.ajax({
        type:"GET",
        url:url,
        data:{id:id},
        dataType:'json',
        beforeSend:function(){
      },
      success:function(response){
        // console.log(response);
        if (response.status == true) {
          swal("Success", response.message, "success");
          get_city();
        }else{
          console.log(response);
        }
      },
      error: function(error){
        console.log(error);
      }
      });
    });
  }

  function edit_city(_this){
    var data = JSON.parse($(_this).attr('data-info'));
    $('#municipality_id').val(data.municipality_id);
    $('#municipality_name').val(data.municipality_name);
    if (data.icon !== null && data.icon !== '') {
      $("#avatar_preview").attr('src', data.icon);
    }else{
      $("#avatar_preview").attr('src', '../webroot/images/municipal.png');      
    }
    $("#history").val(data.history);
    $("#avatar").val(data.icon);
    $(".modal-municipal").text('Edit Municipality');
    $("#municipal_form_modal").modal({'backdrop' : 'static'});

  }
</script>