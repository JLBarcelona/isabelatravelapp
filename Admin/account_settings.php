<!DOCTYPE html>
<html>
  <title>Account Settings</title>
  <?php include("./Layout/header.php") ?>
  <!-- Header css meta -->
<body class="sidebar-mini layout-fixed">
  <div class="wrapper">
  <!-- navbar -->
  <?php include("./Layout/nav.php") ?>
  <!-- Sidebar -->
  <?php include("./Layout/sidebar.php") ?>
   <section class="content-wrapper">
      <div class="container-fluid">
      <form class="needs-validation" id="sample_form_id" action="#" novalidate>
        <div class="row pt-3">
          <div class="col-sm-6">  
            <div class="card">
              <div class="card-header">Profile</div>
              <div class="card-body">
                <ul class="list-group">
                  <li class="list-group-item">
                    <div class="text-center">
                        <img src="../webroot/img/img.png" style="background-image: url('<?php echo Auth::avatar(); ?>');" class="img-bg profile_pict img-circle elevation-2 img-show-full" data-src="<?php echo Auth::avatar(); ?>" alt="User Image" width="153" id="avatar_preview">
                    </div>
                    <div class="text-center mt-2">
                      <button class="btn btn-success btn-sm" type="button" onclick="show_upload('avatar');">Change Profile Picture</button>
                    </div>
                    <textarea class="hide" name="avatar" id="avatar"><?php echo Auth::avatar(); ?></textarea>
                  </li>
                  <li class="list-group-item"><b>Name:</b> <?php echo Auth::fullname() ?></li>
                  <li class="list-group-item"><b>Birthdate:</b> <?php echo Auth::user('birthdate') ?></li>
                  <li class="list-group-item"><b>Gender:</b> <?php echo Auth::user('gender') ?></li>
                  <li class="list-group-item"><b>Username:</b> <?php echo Auth::user('username') ?></li>
                  <li class="list-group-item"><b>Position:</b> <?php echo Auth::position(); ?></li>
                </ul>
              </div>
              <div class="card-footer"></div>
            </div>
          </div>

          <div class="col-sm-6">  
            <div class="card">
              <div class="card-header">Edit Profile</div>
              <div class="card-body">
                  
              <div class="form-row">
                <input type="hidden" id="user_id" name="user_id" value="<?php echo Auth::user('user_id') ?>" placeholder="" class="form-control" required>
                <div class="form-group col-sm-12">
                  <label>Firstname </label>
                  <input type="text" id="firstname" name="firstname" value="<?php echo Auth::user('firstname') ?>" placeholder="Firstname" class="form-control " required>
                  <div class="invalid-feedback" id="err_firstname"></div>
                </div>
                <div class="form-group col-sm-12">
                  <label>Lastname </label>
                  <input type="text" id="lastname" name="lastname" value="<?php echo Auth::user('lastname') ?>" placeholder="Lastname" class="form-control " required>
                  <div class="invalid-feedback" id="err_lastname"></div>
                </div>
                <div class="form-group col-sm-6">
                  <label>Birthdate </label>
                  <input type="date" id="birthdate" name="birthdate" value="<?php echo Auth::user('birthdate') ?>" placeholder="birthdate" class="form-control " required>
                  <div class="invalid-feedback" id="err_birthdate"></div>
                </div>
                  <div class="form-group col-sm-6">
                    <label>Gender </label>
                    <select id="gender" name="gender" class="form-control ">
                      <option value="" selected="">Select Gender</option>
                      <option value="Male" <?php if (Auth::user('gender') == 'Male'): ?> selected <?php endif ?> >Male</option>
                      <option value="Female" <?php if (Auth::user('gender') == 'Female'): ?> selected <?php endif ?> >Female</option>
                    </select>
                    <div class="invalid-feedback" id="err_gender"></div>
                  </div>
                  <div class="form-group col-sm-12">
                  <label>Address </label>
                  <input type="text" id="address" name="address" value="<?php echo Auth::user('address') ?>" placeholder="Address" class="form-control " required>
                  <div class="invalid-feedback" id="err_address"></div>
                </div>

                
                <div class="form-group col-sm-12">
                  <label>Username </label>
                  <input type="text" id="username" name="username" value="<?php echo Auth::user('username') ?>" placeholder="Username" class="form-control " required>
                  <div class="invalid-feedback" id="err_username"></div>
                </div>

                <div class="form-group col-sm-12">
                  <input type="hidden" value="<?php echo Auth::user('user_type') ?>" id="user_type" name="user_type" placeholder="" class="form-control " required>
                  <div class="invalid-feedback" id="err_user_type"></div>
                </div>

                <div class="col-sm-12 text-right">
                  <button class="btn btn-success" type="submit">Save Changes</button>
                </div>
              </div>
          
              </div>
              <div class="card-footer"></div>
            </div>
          </div>
        </div>
          </form>
      </div>
    </section>
  </div>
</body>
  
      <div class="modal fade" role="dialog" id="modal_upload">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <div class="modal-title">
                Upload
              </div>
              <a href="#" class="close" data-dismiss="modal">&times;</a>
            </div>
            <div class="modal-body">
              <input type="hidden" name="input" id="input">
              <div class="text-center">
                <div id="preview_img"></div>
              </div>
              <div class="custom-file">
                <input type="file" class="custom-file-input" onchange="previewFile();" accept="image/*" id="file" name="file" required>
                <label class="custom-file-label" for="validatedCustomFile">Choose file</label>
              </div>
            </div>
            <div class="modal-footer text-right">
              <button class="btn btn-default" onclick="close();" data-dismiss="modal">Close</button>
              <button class="btn btn-dark" onclick="upload();">Upload</button>
            </div>
          </div>
        </div>
      </div>

  <!-- Footer Scripts -->
  <?php include("./Layout/footer.php") ?>
  <script src="../webroot/js/upload.js"></script>
</html>

<script type="text/javascript">
$("#security_question1").val('<?php echo Auth::check('security_question1') ?>');
$("#security_question2").val('<?php echo Auth::check('security_question2') ?>');

    $("#sample_form_id").on('submit', function(e){
    // var url = $(this).attr('action');
    var mydata = $(this).serialize();
    e.stopPropagation();
    e.preventDefault(e);
    // console.log(mydata);
    $.ajax({
      type:"POST",
      url:url_user+'?action=update_user',
      data:mydata,
      cache:false,
      dataType: 'json',
      beforeSend:function(){
          //<!-- your before success function -->
      },
      success:function(response){
          // console.log(response);
        if(response.status == true){
          // console.log(response);
          // show_user();
          swal("Success", "Account updated successfully!", "success");
          setTimeout(function(){
            location.reload();
          }, 1000);
          // $("#modal_add_midwife").modal('hide');
          showValidator(response.error,'sample_form_id');
        }else{
          //<!-- your error message or action here! -->
          showValidator(response.error,'sample_form_id');
        }
      },
      error:function(error){
        console.log(error)
      }
    });
  });

</script>


    
