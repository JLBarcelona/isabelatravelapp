<!DOCTYPE html>
<html>
<title>Category</title>
  <?php include("./Layout/header.php") ?>
  <!-- Header css meta -->
<body class="" onload="active_nav('category'); get_category();">
  <div class="wrapper">
  <!-- navbar -->
  <?php include("./Layout/nav.php") ?>
   <section class="content-page">
      <div class="container-fluid">
        <div class="row pt-3">
          <div class="col-sm-12">
              <div class="card">
              <div class="card-header bg-primary">
                <span class="h4"><i class="fa fa-map-pin"></i> Category</span>
                <button class="btn btn-sm btn-dark float-right" onclick="add_Category()"><i class="fa fa-plus"></i> Add Category</button>
              </div>
              <div class="card-body">
                 <table class="table table-bordered dt-responsive nowrap" id="tbl_category" style="width: 100%;"></table>
              </div>
              <div class="card-footer"></div>
            </div>
            
          </div>
        </div>
      </div>
    </section>
  </div>
</body>
 <div class="modal fade" role="dialog" id="category_form_modal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title modal-category">
            </div>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <form class="needs-validation" id="category_form" action="#" novalidate>
            <div class="form-row">
              <input type="hidden" id="category_id" name="category_id" placeholder="" class="form-control" required>
               <div class="form-group col-sm-12">
                 <div class="text-center">
                        <img src="../webroot/images/festival.png" alt="User Image" width="153" id="avatar_preview">
                    </div>
                    <div class="text-center mt-2">
                      <button class="btn btn-success btn-sm" type="button" onclick="show_upload('avatar');">Add Icon</button>
                    </div>
                    <textarea class="hide" name="avatar" id="avatar"></textarea>
              </div>
              <div class="form-group col-sm-12">
                <label>Category Name </label>
                <input type="text" id="category_name" name="category_name" placeholder="Enter Category" class="form-control " required>
                <div class="invalid-feedback" id="err_category_name"></div>
              </div>

              <div class="form-group col-sm-12">
                <label>Button Color </label>
                <input type="color" id="button_color" name="button_color" placeholder="Enter Category" class="form-control " required>
                <div class="invalid-feedback" id="err_button_color"></div>
              </div>

              <div class="form-group col-sm-12">
                <label>Button Font Color </label>
                <input type="color" id="text_color" name="text_color" placeholder="Enter Category" class="form-control " required>
                <div class="invalid-feedback" id="err_text_color"></div>
              </div>
              <!-- button_color -->
              <div class="col-sm-12 text-right">
                <button class="btn btn-success btn-sm" type="submit">Save</button>
              </div>
            </div>
          </form>
          </div>
          <div class="modal-footer">
            
          </div>
        </div>
      </div>
    </div>

      <div class="modal fade" role="dialog" id="modal_upload">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <div class="modal-title">
                Upload
              </div>
              <a href="#" class="close" data-dismiss="modal">&times;</a>
            </div>
            <div class="modal-body">
              <input type="hidden" name="input" id="input">
              <div class="text-center">
                <div id="preview_img"></div>
              </div>
              <div class="custom-file">
                <input type="file" class="custom-file-input" onchange="previewFile();" accept="image/*" id="file" name="file" required>
                <label class="custom-file-label" for="validatedCustomFile">Choose file</label>
              </div>
            </div>
            <div class="modal-footer text-right">
              <button class="btn btn-default" onclick="close();" data-dismiss="modal">Close</button>
              <button class="btn btn-dark" onclick="upload();">Upload</button>
            </div>
          </div>
        </div>
      </div>
  <!-- Footer Scripts -->
  <?php include("./Layout/footer.php") ?>
  <script src="../webroot/js/upload2.js"></script>

</html>

<script type="text/javascript">
  function add_Category(){
    $(".modal-category").text('Add Category');
    $("#category_form_modal").modal({'backdrop' : 'static'});
    $('#category_id').val('');
    $('#category_name').val('');
    $("#avatar").val('');
    $("#button_color").val('');
    $("#text_color").val('');
    $("#avatar_preview").attr('src', '../webroot/images/festival.png');      
  }
</script>


<!-- Javascript Function-->
<script>
  var tbl_category;
  function show_category(data){
    if (tbl_category) {
      tbl_category.destroy();
    }
    tbl_category = $('#tbl_category').DataTable({
    pageLength: 10,
    responsive: true,
    // ajax: url,
    data: data,
    deferRender: true,
    language: {
    "emptyTable": "No data available"
  },
    columns: [{
    className: '',
    "data": "category_name",
    "title": "Category Name",
  },{
    className: 'width-1 text-center',
    "data": "category_id",
    "orderable": false,
    "title": "Options",
      "render": function(data, type, row, meta){
        var param_data = JSON.stringify(row);
        newdata = '';
        newdata += '<button class="btn btn-success btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="edit_category(this)" type="button"><i class="fa fa-edit"></i></button>';
        newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" data-id=\' '+row.category_id+'\' onclick="delete_category(this)" type="button"><i class="fa fa-trash"></i></button>';
        return newdata;
      }
    }
  ]
  });
  }

  function get_category(){
    var url = url_user + '?action=show_category';
    $.ajax({
        type:"GET",
        url:url,
        data:{},
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
          show_category(response.data)
        },
        error: function(error){
          console.log(error);
        }
      });

  }

  $("#category_form").on('submit', function(e){
    // var url = $(this).attr('action');
    var url = url_user + '?action=add_category';
    var mydata = $(this).serialize();
    e.stopPropagation();
    e.preventDefault(e);

    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      dataType:'json',
      beforeSend:function(){
          //<!-- your before success function -->
      },
      success:function(response){
          //console.log(response)
        if(response.status == true){
          // console.log(response)
          swal("Success", response.message, "success");
          showValidator(response.error,'category_form');
          get_category();
          $("#category_form_modal").modal('hide');
        }else{
          //<!-- your error message or action here! -->
          showValidator(response.error,'category_form');
        }
      },
      error:function(error){
        console.log(error)
      }
    });
  });

  function delete_category(_this){
    var id = $(_this).attr('data-id');
    var url =  url_user + '?action=delete_category';
      swal({
        title: "Are you sure?",
        text: "Do you want to delete this category?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnConfirm: false
      },
      function(){
        $.ajax({
        type:"GET",
        url:url,
        data:{id:id},
        dataType:'json',
        beforeSend:function(){
      },
      success:function(response){
        // console.log(response);
        if (response.status == true) {
          swal("Success", response.message, "success");
          get_category();
        }else{
          console.log(response);
        }
      },
      error: function(error){
        console.log(error);
      }
      });
    });
  }

  function edit_category(_this){
    var data = JSON.parse($(_this).attr('data-info'));
    if (data.icon !== null && data.icon !== '') {
      $("#avatar_preview").attr('src', data.icon);
    }else{
      $("#avatar_preview").attr('src', '../webroot/images/festival.png');      
    }
    $("#avatar").val(data.icon);
    $('#category_id').val(data.category_id);
    $('#category_name').val(data.category_name);
    $(".modal-category").text('Edit Category');
    $("#category_form_modal").modal({'backdrop' : 'static'});
    $("#button_color").val(data.button_color);
    $("#text_color").val(data.text_color);
  }
</script>