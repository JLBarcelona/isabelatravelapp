<!DOCTYPE html>
<html>
<title>Places</title>
  <?php include("./Layout/header.php") ?>
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.2.0/dist/leaflet.css" />
  <!-- Header css meta -->
  <style type="text/css">
    #mapid {
    height: 500px;
    width: 100%;
   }
  </style>
<body class="" onload="active_tab('places'); get_location();">
  <div class="wrapper">
  <!-- navbar -->
  <?php include("./Layout/nav.php") ?>
   <section class="content-page">
      <div class="container-fluid">
        <div class="row pt-3">
          <div class="col-sm-12">
              <div class="card">
              <div class="card-header bg-primary">
                <span class="h4"><i class="fa fa-map-marker-alt"></i> Places</span>
                <button class="btn btn-sm btn-dark float-right" onclick="add_places();"><i class="fa fa-plus"></i></button>
              </div>
              <div class="card-body">
                <table class="table table-bordered dt-responsive nowrap" id="tbl_location" style="width: 100%;"></table>
              </div>
              <div class="card-footer"></div>
            </div>
            
          </div>
        </div>
      </div>
    </section>
  </div>
</body>

<!-- https://www.google.com/maps/dir/16.7197503,121.5678336/16.6733797,121.5530677/@16.6854656,121.552896,13z -->

 <div class="modal fade" role="dialog" id="modal_add_places" style="overflow-y: scroll !important;">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title">
            Add Tourist Spot
            </div>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
           <form class="needs-validation" id="location_form" action="#" novalidate>
                  <div class="form-row">
                    <input type="hidden" id="location_id" name="location_id" placeholder="" class="form-control" required>
                    <div class="form-group col-sm-12">
                      <label>Location Name </label>
                      <input type="text" id="location_name" name="location_name" placeholder="Location Name" class="form-control " required>
                      <div class="invalid-feedback" id="err_location_name"></div>
                    </div>
                    <div class="form-group col-sm-6">
                       <label>Category</label>
                      <select id="category_id" name="category_id" class="form-control "></select>
                      <div class="invalid-feedback" id="err_category_id" data-custom-validator="Category is required!"></div>
                    </div>
                    <div class="form-group col-sm-6">
                      <label>Municipality</label>

                      <select id="municipality_id" name="municipality_id" class="form-control "></select>
                      <div class="invalid-feedback" id="err_municipality_id" data-custom-validator="Municipality is required!"></div>
                    </div>
                    <div class="form-group col-sm-12">
                      <label>Description </label>
                      <textarea class="form-control"  id="description" name="description" placeholder="Write some description" required=""></textarea>
                      <div class="invalid-feedback" id="err_description"></div>
                    </div>
                    <div class="form-group col-sm-6">
                      <label>Lattitude </label>
                      <input type="text" id="lattitude" name="lattitude" placeholder="Lattitude" class="form-control " required>
                      <div class="invalid-feedback" id="err_lattitude"></div>
                    </div>
                    <div class="form-group col-sm-6">
                      <label>Longitude </label>
                      <input type="text" id="longitude" name="longitude" placeholder="Longitude" class="form-control " required>
                      <div class="invalid-feedback" id="err_longitude"></div>
                    </div>
                    <div class="form-group col-sm-12">
                      <button class="btn btn-primary btn-block" type="button" onclick="open_map_modal_select();">Select Location From Map</button>
                    </div>
                    <div class="col-sm-12 form-group">
                      <textarea name="gallery_path" id="gallery_path" cols="30" rows="10" class="hide"></textarea>
                      <div id="gallery_path_preview" class="row">
                        
                      </div>
                      <button class="btn btn-3-2 btn-dark btn-circle" type="button" onclick="show_upload('gallery_path');">Add Gallery</button>
                    </div>
                    <div class="col-sm-12 text-right">
                      <button class="btn btn-success" type="submit">Save</button>
                    </div>
                  </div>
              </form>
          </div>
          <div class="modal-footer">
            
          </div>
        </div>
      </div>
    </div>

 <div class="modal fade" role="dialog" id="modal_open_map">
      <div class="modal-dialog  modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title">
            Select Location
            </div>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-sm-12">
                   <div id="mapid"></div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-danger btn-sm" onclick="remove_marker()">Remove Marker</button>
            <button class="btn btn-dark btn-sm" data-dismiss="modal">Cancel</button>
            <button class="btn btn-success btn-sm" onclick="select_location();">Done</button>
          </div>
        </div>
      </div>
    </div>

 <div class="modal fade" role="dialog" id="modal_upload">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <div class="modal-title">
            Upload
          </div>
          <a href="#" class="close" data-dismiss="modal">&times;</a>
        </div>
        <div class="modal-body">
          <input type="hidden" name="input" id="input">
          <div class="text-center">
            <div id="preview_img"></div>
          </div>
          <div class="custom-file">
            <input type="file" class="custom-file-input" onchange="previewFile();" accept="image/*" id="file" name="file" required>
            <label class="custom-file-label" for="validatedCustomFile">Choose file</label>
          </div>
        </div>
        <div class="modal-footer text-right">
          <button class="btn btn-default" onclick="close();" data-dismiss="modal">Close</button>
          <button class="btn btn-dark" onclick="upload();">Upload</button>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer Scripts -->
  <?php include("./Layout/footer.php") ?>
  <script src="../webroot/js/upload.js"></script>
  <script src="https://unpkg.com/leaflet@1.2.0/dist/leaflet.js"></script>
  <!-- Summernote -->
</html>
<!-- Map -->
<script>

  var lat = '';
  var lon = '';

function show_all_gallery(id){
  let url = url_user+'?action=get_gallery_from_location';
  $.ajax({
      type:"POST",
      url:url,
      data:{id:id},
      dataType:'json',
      beforeSend:function(){
      },
      success:function(response){
        console.log(response);
        $("#gallery_path_preview").html('');
        let datas = response.data;

        let output = datas.map(dt =>{
            let img = '';
            img +='<div class="col-sm-3 mb-1 mt-1 col-6" id="image_uploaded_'+dt.gallery_id+'">';
            img +='<textarea name="gallery_description['+dt.gallery_id+']" id="gallery_description_'+dt.gallery_id+'" cols="30" class="form-control" placeholder="Write Description">'+dt.image_description+'</textarea>';
            img += '<img src="../webroot/img/img.png" class="img-fluid img-bg img-thumbnail" style="background-image:url('+dt.uploaded_image+')" alt="" width="225">';
            img +='<button class="btn btn-block btn-danger btn-sm" type="button" onclick="delete_image_list(\'image_uploaded_'+dt.gallery_id+'\')">Remove</button>';
            img +='<textarea name="gallery_photo['+dt.gallery_id+']" id="gallery_photo_'+dt.gallery_id+'" cols="30" class="hide">'+dt.uploaded_image+'</textarea>';
            img +='</div>';
          return img;
        });

      
        $("#gallery_path_preview").append(output);


      },
      error: function(error){
        console.log(error);
      }
    });
}

function open_map_modal_select(){
  $("#modal_open_map").modal({'backdrop':'static'});
  open_map();
}

function open_map(lattitude_ = 16.6830107, longitude_ = 121.528243){
   var mymap = L.map('mapid').setView([lattitude_, longitude_], 13);
  var marker;
   
     marker = L.marker([lattitude_, longitude_]).addTo(mymap);



    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1Ijoiam9obmx1aXNiMTQiLCJhIjoiY2ttMGRkcXU0MDdsbzJ4cXQxcXdvcWRiYyJ9.8sgOoNJ0iGhLFUlzFIrmVw'
    }).addTo(mymap);


      // Script for adding marker on map click
      function onMapClick(e) {

          if (marker) {
            mymap.removeLayer(marker);          
          }


          marker = L.marker(e.latlng, {
              draggable: true,
              title: "Resource location",
              alt: "Resource Location",
              riseOnHover: true
          }).addTo(mymap)
              .bindPopup(e.latlng.toString()).openPopup();

          marker.once("dragend", function (ev) {
              var chagedPos = ev.target.getLatLng();
              this.bindPopup(chagedPos.toString()).openPopup();
               lat = chagedPos.lat;
               lon = chagedPos.lng;
          });

          var upda =  e.latlng;
              lat = upda.lat;
              lon = upda.lng;
      }

      mymap.on('click', onMapClick);

}

 function select_location(){
    $("#lattitude").val(lat);
    $("#longitude").val(lon);
    $("#modal_open_map").modal('hide');
  }

  function remove_marker(){
     mymap.removeLayer(marker);
     lat = '';
     lon = '';
  }

</script>

<!-- 
<div id="places_gallery"></div>
<button class="btn btn-success btn-sm" type="button" onclick="show_upload('avatar');">Change Profile Picture</button>
<textarea class="hide" name="avatar" id="avatar"><?php echo Auth::avatar(); ?></textarea> -->

<script>

  function add_places(){
    $('#location_id').val('');
    $('#location_name').val('');
    $('#description').val('');
    $('#lattitude').val('');
    $('#longitude').val('');
    $('#category_id').val('');
    $('#municipality_id').val('');
    $("#gallery_path_preview").html('');
    $("#modal_add_places").modal({'backdrop':'static'});
  } 

  var tbl_location;
  

    function get_location(){
    var url = url_user + '?action=show_location';
    $.ajax({
        type:"GET",
        url:url,
        data:{},
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
          show_location(response.data)
        },
        error: function(error){
          console.log(error);
        }
      });

  }

  function show_location(data){
    if (tbl_location) {
      tbl_location.destroy();
    }
    tbl_location = $('#tbl_location').DataTable({
    pageLength: 10,
    responsive: true,
    data: data,
    deferRender: true,
    language: {
    "emptyTable": "No data available"
  },
    columns: [{
    className: '',
    "data": "location_name",
    "title": "Location Name",
  },{
    className: '',
    "data": "description",
    "title": "Description",
  },{
    className: '',
    "data": "lattitude",
    "title": "Lattitude",
  },{
    className: '',
    "data": "longitude",
    "title": "Longitude",
  },{
    className: '',
    "data": "category_name",
    "title": "Category",
  },{
    className: '',
    "data": "municipality_name",
    "title": "Municipality",
  },{
    className: 'width-option-1 text-center',
    "data": "location_id",
    "orderable": false,
    "title": "Options",
      "render": function(data, type, row, meta){
        var param_data = JSON.stringify(row);
        newdata = '';
        newdata += '<button class="btn btn-success btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="edit_location(this)" type="button"><i class="fa fa-edit"></i> </button>';
        newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" onclick="delete_location(\''+row.location_id+'\')" type="button"><i class="fa fa-trash"></i> </button>';
        return newdata;
      }
    }
  ]
  });
  }

  $("#location_form").on('submit', function(e){
    // var url = $(this).attr('action');
    var url = url_user + '?action=add_location';
    var mydata = $(this).serialize();
    e.stopPropagation();
    e.preventDefault(e);

    $.ajax({
      type:"POST",
      url:url,
      data:mydata,
      cache:false,
      dataType:'json',
      beforeSend:function(){
          //<!-- your before success function -->
      },
      success:function(response){
          //console.log(response)
        if(response.status == true){
          console.log(response)
          swal("Success", response.message, "success");
          showValidator(response.error,'location_form');
        }else{
          //<!-- your error message or action here! -->
          showValidator(response.error,'location_form');
        }
      },
      error:function(error){
        console.log(error)
      }
    });
  });

  get_category('category_id');
  get_municipality('municipality_id');
  function get_category(selector){
    let displayer = $("#"+selector);
    $.ajax({
        type:"GET",
        url:url_user+'?action=show_category',
        data:{},
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          // console.log(response.data);
          let output ='<option value="">Select Category</option>';
          let dt = response.data;
          let out = dt.map( opt =>{
            // output += '';
            return '<option value='+opt.category_id+'>'+opt.category_name+'</option>';
          });

          displayer.html(output+out);
        },
        error: function(error){
          console.log(error);
        }
      });
  }

  function get_municipality(selector){
    let displayer = $("#"+selector);
    $.ajax({
        type:"GET",
        url:url_user+'?action=show_city',
        data:{},
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          // console.log(response.data);
          let output ='<option value="">Select Municipality</option>';
          let dt = response.data;
          let out = dt.map( opt =>{
            // output += '';
            return '<option value='+opt.municipality_id+'>'+opt.municipality_name+'</option>';
          });

          displayer.html(output+out);
        },
        error: function(error){
          console.log(error);
        }
      });
  }

  function delete_location(id){
    let url = url_user+'?action=delete_location';
      swal({
        title: "Are you sure?",
        text: "Do you want to delete this location?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnConfirm: false
      },
      function(){
        $.ajax({
        type:"POST",
        url:url,
        data:{id : id},
        dataType:'json',
        beforeSend:function(){
      },
      success:function(response){
        // console.log(response);
        if (response.status == true) {
          swal("Success", response.message, "success");
          get_location();
        }else{
          console.log(response);
        }
      },
      error: function(error){
        console.log(error);
      }
      });
    });
  }

  function edit_location(_this){
    var data = JSON.parse($(_this).attr('data-info'));
    $('#location_id').val(data.location_id);
    $('#location_name').val(data.location_name);
    $('#description').val(data.description);
    $('#lattitude').val(data.lattitude);
    $('#longitude').val(data.longitude);
    $('#category_id').val(data.category_id);
    $('#municipality_id').val(data.municipality_id);
    $("#modal_add_places").modal({'backdrop': 'static'});
    lat = data.lattitude;
    lon = data.longitude;
    open_map(lat, lon);
    show_all_gallery(data.location_id);
   }
</script>