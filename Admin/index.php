<!DOCTYPE html>
<html>
<title>Dashboard</title>
  <link rel="stylesheet" href="../webroot/plugins/ekko-lightbox/ekko-lightbox.css">
  <?php include("./Layout/header.php") ?>
  <!-- Header css meta -->
<body class="" onload="show_menus(); show_gallery();">
  <div class="wrapper">
  <!-- navbar -->
  <?php include("./Layout/nav.php") ?>
   <section class="content-page">
      <div class="container-fluid">
        <div class="row pt-3">
          <!-- Card -->
          <div class="col-sm-4 col-12">
              <div class="card bg-success">
              <div class="card-body">
                  <span class="h4"><i class="fa fa-map"></i> Municipality</span> <span class="float-right h4 Municipality_counters">0</span>     
              </div>
              <div class="card-footer">
                <a href="./city.php" class="btn btn-link text-white">View More</a>
              </div>
            </div>
          </div>
          <!-- Card End -->
           <!-- Card -->
          <div class="col-sm-4 col-6">
              <div class="card bg-danger">
              <div class="card-body">
                  <span class="h4"><i class="fa fa-map-pin"></i> Category</span> <span class="float-right h4 Category_counters">0</span>     
              </div>
              <div class="card-footer">
                <a href="./category.php" class="btn btn-link text-white">View More</a>
              </div>
            </div>
          </div>
          <!-- Card End -->

           <!-- Card -->
          <div class="col-sm-4 col-6">
              <div class="card bg-primary">
              <div class="card-body">
                  <span class="h4"><i class="fa fa-map-marker-alt"></i> Places</span> <span class="float-right h4 Places_counters">0</span>     
              </div>
              <div class="card-footer">
                <a href="./places.php" class="btn btn-link text-white">View More</a>
              </div>
            </div>
          </div>
          <!-- Card End -->
          <div class="col-sm-12 col-12">
            
            <div class="card">
              <div class="card-header h4 bg-dark"><i class="fa fa-images"></i> Gallery</div>
              <div class="card-body">
                <div>
                    <div class="h5">Municipality</div>
                   <div class="btn-group w-100 mb-2" id="menus_filter_city">
                    <!-- city filters -->
                  </div>
                    <div class="h5">Category</div>
                  <div class="btn-group w-100 mb-2" id="menus_filter_category">
                    <!-- category filter -->
                  </div>
                </div>
                <div>
                  <div class="filter-container p-0 row" id="gallery_menus">
        
                  </div>
                </div>

              <div class="card-footer"></div>
            </div>
          </div>

        </div>
      </div>
    </section>
  </div>
</body>
  <!-- Footer Scripts -->
  <?php include("./Layout/footer.php") ?>
  <!-- Ekko Lightbox -->
<script src="../webroot/plugins/ekko-lightbox/ekko-lightbox.min.js"></script>
<!-- Filterizr-->
<script src="../webroot/plugins/filterizr/jquery.filterizr.min.js"></script>
</html>
<script>
  function show_menus(){
    let url = url_user+'?action=show_menus';
    $.ajax({
        type:"GET",
        url:url,
        data:{},
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
          let selector_city = $("#menus_filter_city");
          let selector_cat = $("#menus_filter_category");
          let category = response.category;
          let city = response.city;

          selector_cat.append('<a class="btn btn-info active" href="javascript:void(0)" data-filter="all"> All items </a>');
          let output1 = category.map(cat => {
            let cats = '<a class="btn btn-info" href="javascript:void(0)" data-filter="cat_'+cat.category_id+'"> '+cat.category_name+' </a>';
            selector_cat.append(cats);
          });

           let output2 = city.map(mun => {
              let citys =  '<a class="btn btn-info" href="javascript:void(0)" data-filter="mun_'+mun.municipality_id+'"> '+mun.municipality_name+' </a>';
              selector_city.append(citys);
            });
        },
        error: function(error){
          console.log(error);
        }
      });
  }

  function show_gallery(){
    let url = url_user+'?action=show_gallery';
    $.ajax({
        type:"GET",
        url:url,
        data:{},
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          console.log(response);
          let selector = $("#gallery_menus");
          let list = response.data;
          let output = list.map(gal => {
            let outputs = ` <div class="filtr-item mt-2 mb-2 col-sm-2 col-6" data-category="cat_`+gal.category_id+`, mun_`+gal.municipality_id+`" data-sort="white sample">
                      <a href="`+gal.uploaded_image+`" data-toggle="lightbox" data-title="`+gal.image_description+`">
                        <img src="../webroot/img/img.png" style="background-image: url(`+gal.uploaded_image+`);" class="img-fluid img-bg mb-2" alt="white sample" width="260" />
                      </a>
                    </div>`
            selector.append(outputs);
          })
        },
        error: function(error){
          console.log(error);
        }
      });
  }

  function get_counters(){
    let url = url_user + '?action=show_counters';
    $.ajax({
        type:"GET",
        url:url,
        data:{},
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
          // console.log(response);
          $(".Municipality_counters").text(response.city);
          $(".Category_counters").text(response.category);
          $(".Places_counters").text(response.places);
        },
        error: function(error){
          console.log(error);
        }
      });
  }

  get_counters();
  setInterval(function(){
    get_counters();
  },5000);

setTimeout(function(){
    $(function () {
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
      event.preventDefault();
      $(this).ekkoLightbox({
        alwaysShowClose: true
      });
    });

    $('.filter-container').filterizr({gutterPixels: 3});
    $('.btn[data-filter]').on('click', function() {
      $('.btn[data-filter]').removeClass('active');
      $(this).addClass('active');
    });
  })
},1000);
</script>