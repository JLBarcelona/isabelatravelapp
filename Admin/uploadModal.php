<div class="modal fade" role="dialog" id="modal_upload">
	<div class="modal-dialog">
	  <div class="modal-content">
	    <div class="modal-header">
	      <div class="modal-title">
	        Upload
	      </div>
	      <a href="#" class="close" data-dismiss="modal">&times;</a>
	    </div>
	    <div class="modal-body">
	      <div class="text-center">
	        <div id="preview_img"></div>
	      </div>
	      <div class="custom-file">
	        <input type="file" class="custom-file-input" onchange="previewFile();" accept="image/*" id="file" name="file" required>
	        <label class="custom-file-label" for="validatedCustomFile">Choose file</label>
	      </div>
	    </div>
	    <div class="modal-footer text-right">
	      <button class="btn btn-3-2 btn-success btn-sm theme-button-success mr-1" onclick="close();" data-dismiss="modal">Close</button>
	      <button class="btn btn-3-2 btn-success btn-sm theme-button-success mr-1" onclick="upload();">Upload</button>
	    </div>
	  </div>
	</div>
</div>





<script type="text/javascript">
  let i = 0;
  
  function show_upload(param){
	    // Preview
	    $("#input").val(param);
	    $("#modal_upload").modal('show');
	  }

	  function previewFile() {
	    const att = $("#input").val();
	    const preview = $("#"+att+"_preview");
	    const base64 = $("#"+att);
	    const file = document.querySelector('input[type=file]').files[0];
	    const reader = new FileReader();

	    reader.addEventListener("load", function () {
	      base64.val(reader.result);
	      $("#preview_img").html('<img src="'+reader.result+'" alt="" class="img-fluid img-thumbnail animated fadeIn mb-2">');
	      // $("#preview_img").attr('style','background-image: url(\''+reader.result+'\');');
	    }, false);

	    if (file) {
	      reader.readAsDataURL(file);
	    }
	  }


	  function upload(){
	      const att = $("#input").val();
	      const preview = $("#"+att+"_preview");
	      const base64 = $("#"+att).val();
	      let output = '';
	     
	     // preview.html('<img src="'+base64+'" alt="" class="img-fluid img-thumbnail animated fadeIn mt-2">');
	     // preview.attr('style','background-image: url(\''+base64+'\');');
		
	    let tmp_ids = 'img_id_'+i;
		output += '<div class="theme-col-3" id="'+tmp_ids+'">';
		output += '<div class="row theme-row mb-1">';
		output += '<div class="theme-col-3">';
		output += '<select class="theme-input theme-input theme-input-4">';
		output += '<option>外観</option>';
		output += '<option>エントランス</option>';
		output += '<option>部屋</option>';
		output += '<option>廊下</option>';
		output += '<option>駐車場</option>';
		output += '<option>宅配ボックス</option>';
		output += '<option>貯水タンク</option>';
		output += '<option>ポンプ</option>';
		output += '<option>浄化槽</option>';
		output += '<option>その他</option>';
		output += '</select>';
		output += '</div>';
		output += '</div>';
		output += '<div class="row theme-row mb-1">';
		output += '<div class="theme-col-3">';
		output += '<button class="btn btn-3 btn-success btn-sm theme-button-success" style="max-width:220px;">ダウンロード</button>';
		output += '<button class="btn btn-3 btn-danger btn-sm theme-button-danger ml-1" onclick="delete_imge(\''+tmp_ids+'\')" style="max-width:220px;">削除</button>';
		output += '</div>';
		output += '</div>';
		output += '<div class="row theme-row">';
		output += '<div class="theme-col-3">';
		output += '<img src="<?php echo $this->Html->webroot('main/img/img.png') ?>" class="img-fluid img-bg" style="background-image:url('+base64+')" alt="" width="225">';
		output += '</div>';
		output += '</div>';
		output += '<div class="row theme-row mt-1">';
		output += '<div class="theme-col-4">';
		output += '<input type="text input" class="theme-input theme-input-4">';
		output += '</div>';
		output += '</div>';
		output += '</div>';

		i++;

		$("#modal_upload").modal('hide');
		$("#input").val('');
		$("#preview_img").html('');
		$("#profile_path").val('');


         $("#profile_path_preview").append(output);
	  }

	  function close(){
	     $("#modal_upload").modal('hide');
	     $("#input").val('');
	       $("#preview_img").html('');
	  }
	 

	  function delete_imge(ids){
	  	$("#"+ids).hide('fast');
	  }






</script>