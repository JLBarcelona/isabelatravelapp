<?php if (Auth::check()): ?>
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <!-- <a class="navbar-brand" href="#">Navbar</a> -->
  <a href="./" class="navbar navbar-brand bold"><span class=""><img src="../webroot/img/logo.png" class="img-fluid mr-2" width="35" alt=""> ISABELA TRAVEL APP</span></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav ml-auto">
        <a href="./city.php" class="nav-link" id="city"><i class="fa fa-map"></i> Municipality</a>
        <a href="./category.php" class="nav-link" id="category"><i class="fa fa-map-pin"></i> Category</a>
        <a href="./places.php" class="nav-link" id="places"><i class="fa fa-map-marker-alt"></i> Places</a>
        <a class="nav-link" data-toggle="dropdown" href="#">
          <!-- <i class="fas fa-user-circle"></i> -->
             <img src="../webroot/img/img.png" style="background-image: url('<?php echo '../'.trim(Auth::avatar()) ?>
          ');" class="img-bg profile_pict img-circle elevation-1" width="25" alt="User Image">
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header"><?php echo Auth::fullname(); ?></span>
          <div class="dropdown-divider"></div>
       <!--    <a href="account_settings.php" class="dropdown-item">
            <i class="fas fa-shield-alt mr-2"></i> Account Settings
          </a> -->
          <div class="dropdown-divider"></div>
            <a href="logout_user.php" class="dropdown-item">
            <i class="fas fa-sign-out-alt"></i> Logout
          </a>
        </div>
    </div>
  </div>
</nav>
  <?php else: ?>
    <?php //@header('location:../') ?>
<?php endif ?>
