<script src="../webroot/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="../webroot/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables -->
<script src="../webroot/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../webroot/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="../webroot/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="../webroot/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<!-- AdminLTE App -->
<script src="../webroot/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../webroot/dist/js/demo.js"></script>
<script src="../webroot/js/main.js"></script>
<script src="../webroot/js/sweetalert.min.js"></script>
<script src="../webroot/plugins/select2/js/select2.full.min.js"></script>

<div class="modal fade" role="dialog" id="modal_preview">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title">
        Preview
        </div>
        <button class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body p-0">
        <img src="" alt="preview" id="preview_image" class="img-fluid animated fadeIn" style="width: 100%; max-width: 100%;">
      </div>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
const url_user = '../function/function.php';

  function active_tab(tab){
    $("#"+tab).addClass("active");
  }

  function active_tree(tab, tree){
    $("#"+tree).addClass("menu-open active");
    $("#"+tab).addClass("active");
  }
</script>

<script type="text/javascript">
	$(".img-show-full").on('click', function(){
		let img = $(this).attr('data-src');
			$("#preview_image").attr('src', img);
			$("#modal_preview").modal('show');
	});
</script>


<script type="text/javascript">
        function show_children_list(){
          $.ajax({
              type:"GET",
              url:url_user+'?action=children_list',
              data:{},
              dataType:'json',
              beforeSend:function(){
              },
              success:function(response){
                // console.log(response);
               if (response.status == true) {
                  let dt = response.data;
                  let options = dt.map(chld => {
                     let ouput = '';
                    ouput +='<option value="'+chld.child_id+'">'+chld.firstname+" "+chld.lastname+'</option>';
                    return ouput;
                  });

                  // console.log(ouput);

                  $("#child_id").html(options);
               }else{
                console.log(response);
               }
              },
              error: function(error){
                console.log(error);
              }
            });
        }

        function show_vaccines(){
          $.ajax({
              type:"GET",
              url:url_user+'?action=vaccines_list',
              data:{},
              dataType:'json',
              beforeSend:function(){
              },
              success:function(response){
                // console.log(response);
               if (response.status == true) {
                  let dt = response.data;
                  let options = dt.map(vac => {
                     let ouput = '';
                    ouput +='<option value="'+vac.vac_id+'">'+vac.vaccine_name+'</option>';
                    return ouput;
                  });

                  // console.log(ouput);

                  $("#vac_id").html(options);
               }else{
                console.log(response);
               }
              },
              error: function(error){
                console.log(error);
              }
            });
        }

        function active_nav(tab){
           $("#"+tab).addClass("active");

        }

</script>