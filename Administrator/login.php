<!DOCTYPE html>
<html>
<title>Login</title>
  <?php include("Layout/header.php") ?>
  <!-- Header css meta -->
  <style type="text/css">
  
    .bg-homes { 
        background-image: url('../webroot/img/travel.jpg');
        background-size: cover;
        background-repeat: no-repeat;
        background-attachment: fixed;
    }

    .bg-homes::before {
        content: "";
        position: absolute;
        top: 0px;
        right: 0px;
        bottom: 0px;
        left: 0px;
        background-color: rgba(10,14,60,0.5);
    }
  </style>
<body class="bg-homes">
  <div class="">
  <!-- navbar -->
  <?php include("Layout/nav.php") ?>
  <!-- Sidebar -->
   <section class="">
      <div class="container-fluid">
        <div class="row d-flex justify-content-center">
          <div class="col-sm-4 col-md-3 mt-5">
             <form class="needs-validation" id="sample_form_id" novalidate>
              <div class="card shadow">
                <div class="card-body">
                  <div class="text-center">
                     <img src="../webroot/img/logo.png" class="img-fluid" width="100" alt="">
                     <div class="h4 text-center mt-1">ISABELA TRAVEL APP</div>
                  </div>

                    <div class="form-row">
                      <div class="form-group col-sm-12">
                        <label>Username </label>
                        <input type="text" id="username" name="username" placeholder="Username" class="form-control " required>
                        <div class="invalid-feedback" id="err_username"></div>
                      </div>
                      <div class="form-group col-sm-12">
                        <label>Password </label>
                        <input type="password" id="password" name="password" placeholder="Password" class="form-control " required>
                        <div class="invalid-feedback" id="err_password"></div>
                      </div>

                      <div class="col-sm-12 text-right">
                       <button class="btn btn-dark" type="submit">Login</button>
                      </div>
                    </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  </div>
</body>
  <!-- Footer Scripts -->
  <?php include("Layout/footer.php") ?>
</html>

<script type="text/javascript">
  $("#sample_form_id").on('submit', function(e){
    var mydata = $(this).serialize();
    e.stopPropagation();
    e.preventDefault(e);
    // alert(mydata);
    $.ajax({
      type:"POST",
      url:'../function/function.php?action=login',
      data:mydata,
      dataType:'json',
      cache:false,
      beforeSend:function(){
          //<!-- your before success function -->
      },
      success:function(response){
          // console.log(response);
        if(response.status == true){
          let user = response.user.user_type;
          window.location = "../Admin/";
          showValidator(response.error,'sample_form_id');
        }else{
          if (typeof response.message == 'undefined') {
          
          }else{
            swal("Error",response.message, "error");

          }
          // console.log(response.message);
          //<!-- your error message or action here! -->
          showValidator(response.error,'sample_form_id');
        }
      },
      error:function(error){
        console.log(error)
      }
    });
  });

  $(document).ready(function(){
    let i = 1;
    let bg = $(".bg-homes");
    setInterval(function(){
      $(".bg-homes").attr('style', 'background-image:url(../webroot/img/'+i+'.jpg)');
      if (i == 5) {
        i = 1;
      }else{
        i++;
      }
    },5000);
  });
</script>

