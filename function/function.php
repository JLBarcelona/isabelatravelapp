<?php 
require_once("../config/config.php");
require_once("../config/auth.php");
require_once("../core/validator.php");

 
$db = new db();

$action = $_REQUEST['action'];

switch ($action) {
	case 'login':
		$v = new Validator();

		 $v->make($_POST, [
		 	'username' => 'required',
		 	'password' => 'required'
		 ]);

		 $data = array('username' => $_POST['username'], 'password' => $_POST['password']);

		 if (!$v->fail) {
			 	echo $db->json(['status' => false, 'error' => $v->message]);
		 }else{
			 	echo $db->login('tbl_admin', $data);
		 }
	break;
	
	case 'show_location':
		$array = array();
		$query = "SELECT a.*,b.*,c.* from tbl_location a
		INNER JOIN tbl_category b on a.category_id = b.category_id
		INNER JOIN tbl_municipality c on a.municipality_id = c.municipality_id
		where a.deleted_at is null order by a.location_id desc";
		$res = $db->get($array,$query);
		echo $db->json(['status' => true, 'data' => $res]);
	break;

	case 'show_menus':
		$array = array();
		$query = "SELECT * from tbl_municipality where deleted_at is null order by municipality_name asc";
		$res = $db->get($array,$query);

		$query2 = "SELECT * from tbl_category where deleted_at is null order by category_name asc";
		$cat_res = $db->get($array,$query2);

		echo $db->json(['status' => true, 'city' => $res, 'category' => $cat_res]);
	break;

	case 'show_menus_category':
		$array = array('municipality_id' => $_GET['city_id']);
		$query2 = "SELECT a.*,b.* from tbl_category a
		INNER JOIN tbl_location b on a.category_id = b.category_id
		where a.deleted_at is null and b.municipality_id = :municipality_id group by a.category_name order by a.category_name asc";
		$cat_res = $db->get($array,$query2);

		echo $db->json(['status' => true, 'category' => $cat_res]);
	break;

	case 'show_menus_category_location':
		$array = array('municipality_id' => $_GET['city_id'], 'category_id' => $_GET['category_id']);
		$query2 = "SELECT * from tbl_location where deleted_at is null and municipality_id = :municipality_id and category_id = :category_id order by location_name asc";
		$cat_res = $db->get($array,$query2);
		
		$data = array();
		$i = 0;
		foreach ($cat_res as $value) {
			# code...
				
			$data3 = array('location_id' => $value->location_id);
			$query3 = "SELECT * from tbl_gallery where location_id = :location_id";
			$gallery_res = $db->get($data3,$query3);
			$data[$i]['category'] = $value;
			$data[$i]['gallery'] = $gallery_res;
			$i++;
		}

		echo $db->json(['status' => true, 'locations' => $data]);
	break;


	case 'show_menus_category_location_search':
		$array = array();
		$fil = $_GET['filters'];
		$filters = (!empty($_GET['filters']))? "and concat(location_name,description) like '%$fil%' " : "";

		$query2 = "SELECT * from tbl_location where deleted_at is null ".$filters."";
		$cat_res = $db->get($array,$query2);
		
		$data = array();
		$i = 0;
		foreach ($cat_res as $value) {
			# code...
				
			$data3 = array('location_id' => $value->location_id);
			$query3 = "SELECT * from tbl_gallery where location_id = :location_id";
			$gallery_res = $db->get($data3,$query3);
			$data[$i]['category'] = $value;
			$data[$i]['gallery'] = $gallery_res;
			$i++;
		}

		echo $db->json(['status' => true, 'locations' => $data]);
	break;

	case 'get_gallery_from_location':
		$array = array('location_id' => $_POST['id']);
		$query = "SELECT * from tbl_gallery where deleted_at is null and location_id=:location_id";
		$res = $db->get($array,$query);
		echo $db->json(['status' => true, 'data' => $res]);
	break;

	case 'show_gallery':
		$array = array();
		$query = "SELECT a.uploaded_image, a.image_description, b.location_name as location, c.category_name as category, c.category_id ,d.municipality_id, d.municipality_name as city from tbl_gallery a
		INNER JOIN tbl_location b on a.location_id = b.location_id
		INNER JOIN tbl_category c on b.category_id = c.category_id
		INNER JOIN tbl_municipality d on b.municipality_id = d.municipality_id
		where a.deleted_at is null";
		$res = $db->get($array,$query);
		echo $db->json(['status' => true, 'data' => $res]);
	break;

	case 'show_counters':
		$city = $db->counter('tbl_municipality','municipality_id','where deleted_at is null');
		$category = $db->counter('tbl_category','category_id','where deleted_at is null');
		$places = $db->counter('tbl_location','location_id','where deleted_at is null');
		echo $db->json(['city' => $city, 'category' => $category, 'places' => $places]);
	break;


	case 'add_location':
		$gallery_count = 0;
		$v = new Validator();

		 $v->make($_POST, [
		 	'location_name' => 'required',
			'category_id' => 'required',
			'municipality_id' => 'required',
			'description' => 'required',
			'lattitude' => 'required',
			'longitude' => 'required'
		 ]);


		 if (!$v->fail) {
			 	echo $db->json(['status' => false, 'error' => $v->message]);
		 }else{
		 	if (!empty($_POST['location_id'])) {
		 		$data = array('location_id' => $_POST['location_id'],
		 					'location_name' => $_POST['location_name'],
							'category_id' => $_POST['category_id'],
							'municipality_id' => $_POST['municipality_id'],
							'description' => $_POST['description'],
							'lattitude' => $_POST['lattitude'],
							'longitude' => $_POST['longitude'],
							'updated_at' => date('Y-m-d H:i:s A'));

		 		$location_id = $_POST['location_id'];
				$array = array('location_id' => $location_id);
				if ($db->delete('tbl_gallery', $array)) {
				}

		 		foreach ($_POST['gallery_photo'] as $key => $value) {
		 			$data_gal = array('location_id' => $_POST['location_id'],
		 							'uploaded_image' => $value,
		 							'image_description' => $_POST['gallery_description'][$key],
		 							'created_at' => date('Y-m-d H:i:s A'));

		 			if ($db->save('tbl_gallery', $data_gal)) {
		 				$gallery_count++;
		 			}
		 		}

		 		if ($db->update('tbl_location', $data)) {
				 	echo $db->json(['status' => true, 'message' => 'Location updated successfully!', 'data' => $gallery_count.' addedd successfully!']);
		 		}
		 	}else{
		 		$data = array('location_name' => $_POST['location_name'],
							'category_id' => $_POST['category_id'],
							'municipality_id' => $_POST['municipality_id'],
							'description' => $_POST['description'],
							'lattitude' => $_POST['lattitude'],
							'longitude' => $_POST['longitude'],
							'created_at' => date('Y-m-d H:i:s A'));

		 		$lastId = $db->saveWithLastId('tbl_location', $data);
		 		// saveWithLastId
		 		if ($lastId > 0) {
		 			foreach ($_POST['gallery_photo'] as $key => $value) {
			 			$data_gal = array('location_id' => $lastId,
			 							'uploaded_image' => $value,
			 							'image_description' => $_POST['gallery_description'][$key],
			 							'created_at' => date('Y-m-d H:i:s A'));

			 			if ($db->save('tbl_gallery', $data_gal)) {
			 				$gallery_count++;
			 			}
			 		}


				 	echo $db->json(['status' => true, 'message' => 'Location saved successfully!', 'data' => $_POST['gallery_photo']]);
		 		}
		 	}
		 }

	break;

	case 'add_city':
		$v = new Validator();

		 $v->make($_POST, [
		 	'municipality_name' => 'required',
		 ]);

		 if (!$v->fail) {
			 	echo $db->json(['status' => false, 'error' => $v->message]);
		 }else{
		 	// $data = array('municipality_id' => $_POST['municipality_id'], 'municipality_name' => $_POST['municipality_name']);

			if (!empty($_POST['municipality_id'])) {
		 		$data = array('municipality_id' => $_POST['municipality_id'], 'icon' => $_POST['avatar'], 'municipality_name' => $_POST['municipality_name'], 'history' => $_POST['history'], 'updated_at' => date('Y-m-d H:i:s A'));
		 		if ($db->update('tbl_municipality', $data)) {
				 	echo $db->json(['status' => true, 'message' => 'Municipality updated successfully!']);
		 		}
			}else{
		 		$data = array('icon' => $_POST['avatar'], 'municipality_name' => $_POST['municipality_name'], 'history' => $_POST['history'], 'updated_at' => date('Y-m-d H:i:s A'));
		 		if ($db->save('tbl_municipality', $data)) {
				 	echo $db->json(['status' => true, 'message' => 'Municipality saved successfully!']);
		 		}
			}

		 }
	break;

	case 'delete_city':
			$municipality_id = $_GET['id'];
			$array = array('municipality_id' => $municipality_id);
			if ($db->delete('tbl_municipality', $array)) {
				echo $db->json(['status' => true, 'message' => 'Municipality deleted successfully!']);
			}
	break;

	case 'show_city':
		$array = array();
		$query = "SELECT * from tbl_municipality where deleted_at is null order by municipality_id desc";
		$res = $db->get($array,$query);
		echo $db->json(['status' => true, 'data' => $res]);
	break;

	case 'find_city':
		$id = $_GET['id'];
		$array = array('id' => $id);
		$query = "SELECT * from tbl_municipality where user_id = :id order by user_id desc";
		$res = $db->find($array,$query);
		echo $db->json(['status' => true, 'data' => $res]);
	break;

	// category

	case 'add_category':
		$v = new Validator();

		 $v->make($_POST, [
		 	'category_name' => 'required',
		 	'button_color' => 'required',
		 	'text_color' => 'required',
		 ]);

		 if (!$v->fail) {
			 	echo $db->json(['status' => false, 'error' => $v->message]);
		 }else{
		 	// $data = array('municipality_id' => $_POST['municipality_id'], 'municipality_name' => $_POST['municipality_name']);




			if (!empty($_POST['category_id'])) {
		 		$data = array('category_id' => $_POST['category_id'], 'category_name' => $_POST['category_name'], 'icon' => $_POST['avatar'], 'button_color' => $_POST['button_color'],'text_color' => $_POST['text_color'], 'updated_at' => date('Y-m-d H:i:s A'));
		 		if ($db->update('tbl_category', $data)) {
				 	echo $db->json(['status' => true, 'message' => 'Category updated successfully!']);
		 		}
			}else{
		 		$data = array('category_name' => $_POST['category_name'], 'icon' => $_POST['avatar'], 'button_color' => $_POST['button_color'],'text_color' => $_POST['text_color'], 'created_at' => date('Y-m-d H:i:s A'));

		 		if ($db->save('tbl_category', $data)) {
				 	echo $db->json(['status' => true, 'message' => 'Category saved successfully!']);
		 		}
			}

		 }
	break;

	case 'delete_category':
			$category = $_GET['id'];
			$array = array('category_id' => $category);
			if ($db->delete('tbl_category', $array)) {
				echo $db->json(['status' => true, 'message' => 'Category deleted successfully!']);
			}
	break;

	case 'delete_location':
		$location_id = $_POST['id'];
			$array = array('location_id' => $location_id);
			if ($db->delete('tbl_location', $array)) {
				echo $db->json(['status' => true, 'message' => 'Location deleted successfully!']);
			}
	break;

	case 'show_category':
		$array = array();
		$query = "SELECT * from tbl_category where deleted_at is null order by category_id desc";
		$res = $db->get($array,$query);
		echo $db->json(['status' => true, 'data' => $res]);
	break;

	case 'find_category':
		$id = $_GET['id'];
		$array = array('id' => $id);
		$query = "SELECT * from tbl_category where user_id = :id order by user_id desc";
		$res = $db->find($array,$query);
		echo $db->json(['status' => true, 'data' => $res]);
	break;

	default:
		# code...
	break;
}

 ?>