<!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary bg-primary  elevation-4">
    <!-- Brand Logo -->
    <!-- Sidebar -->
    <div class="sidebar" >
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="info">
          <a href="list_category_city.php?city=<?php echo $_REQUEST['city'] ?>&city_name=<?php echo $_REQUEST['city_name'] ?>" class="d-block text-white">  <i class="fa fa-arrow-left"></i> <span class="titles">Back to <?php echo $_REQUEST['city_name'] ?> Categories</span></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column city_sidebar" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
