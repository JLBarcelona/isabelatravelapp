<!-- <script src="webroot/plugins/jquery/jquery.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js" integrity="sha512-AFwxAkWdvxRd9qhYYp1qbeRZj6/iTNmJ2GFwcxsMOzwwTaRwz2a/2TX225Ebcj3whXte1WGQb38cXE5j7ZQw3g==" crossorigin="anonymous"></script>
<!-- Bootstrap 4 -->
<script src="webroot/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables -->
<script src="webroot/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="webroot/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="webroot/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="webroot/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<!-- AdminLTE App -->
<script src="webroot/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="webroot/dist/js/demo.js"></script>
<script src="webroot/js/main.js"></script>
<script src="webroot/js/sweetalert.min.js"></script>


<script type="text/javascript">
	 function show_menus(){
	    let urls = url+'?action=show_menus';
	    $.ajax({
	        type:"GET",
	        url:urls,
	        data:{},
	        dataType:'json',
	        beforeSend:function(){
				let loader = ` <div class="col-sm-12 text-center col-12">
										<img src="webroot/img/loading.gif" class="img-fluid animated fadeIn">
						          </div>`;
				$(".menus_filter_city").append(loader);  
	        },
	        success:function(response){
				let selector_city = $(".menus_filter_city");
				let selector_city_sidebar = $(".city_sidebar");
				let selector_cat = $(".menus_filter_category");
				let category = response.category;
				let city = response.city;
        	    setTimeout(function(){
    	    	  // console.log(response);
		          	selector_city.html('');
		           let output2 = city.map(mun => {
		           		$(".img-logo").removeClass('hide');
		              let citys = ` <div class="col-sm-12 col-12  mb-2">
						            <a  href="list_category_city.php?city=`+mun.municipality_id+`&city_name=`+mun.municipality_name+`" class="btn btn-block btn-dark">`+mun.municipality_name+`</a>
						          </div>`;

					  let sidebar_city = `<li class="nav-item">
											<a href="list_category_city.php?city=`+mun.municipality_id+`&city_name=`+mun.municipality_name+`" class="nav-link">
											  <i class="nav-icon fas fa-map"></i>
											  <p>
											    `+mun.municipality_name+`
											  </p>
											</a>
											</li>`;
				      selector_city_sidebar.append(sidebar_city);
		              selector_city.append(citys);
		            });
        	    },2000);
	        },
	        error: function(error){
	          console.log(error);
	        }
	      });
	  }

	  function show_category(city_id,city_name){
	    let urls = url+'?action=show_menus_category';
	    $.ajax({
	        type:"GET",
	        url:urls,
	        data:{city_id : city_id},
	        dataType:'json',
	        beforeSend:function(){
	        	let loader = ` <div class="col-sm-12 text-center col-12">
	          							<img src="webroot/img/loading.gif" class="img-fluid animated fadeIn">
							          </div>`;
				$(".menus_filter_category").append(loader);     
	        },
	        success:function(response){
	          // console.log(response);
	          setTimeout(function(){
          	 	  let selector_city = $(".menus_filter_city");
		          let selector_city_sidebar = $(".city_sidebar");
		          let selector_cat = $(".menus_filter_category");
		          let category = response.category;
		          let city = response.city;
		          	selector_cat.html('');
		          	if (category == '' || category == null) {
		          		let cats = ` <div class="col-sm-12 text-center col-12">
		          							<img src="webroot/img/empty_data_set.jpg" class="img-fluid animated fadeIn">
								          </div>`;
					    selector_cat.append(cats);
		          	}else{
	          		    let output1 = category.map(cat => {
				         	// console.log(cat);
				         	let icons = (cat.icon !== '' && cat.icon !== null) ? cat.icon : 'webroot/images/festival.png';
				         	let cats = ` <div class="col-sm-6 col-12">
						            <a href="list_location.php?city=`+city_id+`&category=`+cat.category_id+`&city_name=`+city_name+`&category_name=`+cat.category_name+`" class="btn btn-block" style="background-color:`+cat.button_color+`; color:`+cat.text_color+`"><img class="img-fluid" width="50" src="`+icons+`"> <div class="h3 ml-3">`+cat.category_name+`</div></a>
						          </div>`;
					      selector_cat.append(cats);
			            });
			        }
	          },2000);
		     },
	        error: function(error){
	          console.log(error);
	        }
	      });
	  }

	    function show_category_side(city_id,city_name){
	    let urls = url+'?action=show_menus_category';
	    $.ajax({
	        type:"GET",
	        url:urls,
	        data:{city_id : city_id},
	        dataType:'json',
	        beforeSend:function(){
	        	let loader = ` <div class="col-sm-12 text-center col-12">
	          							<img src="webroot/img/loading.gif" class="img-fluid animated fadeIn">
							          </div>`;
				$(".menus_filter_category").append(loader);     
	        },
	        success:function(response){
	          // console.log(response);
	          setTimeout(function(){
          	 	  let selector_city = $(".menus_filter_city");
		          let selector_city_sidebar = $(".city_sidebar");
		          let selector_cat = $(".menus_filter_category");
		          let category = response.category;
		          let city = response.city;
		          	
		          	  let output1 = category.map(cat => {
				         	// console.log(cat);
				         	let sidebar_city = `<li class="nav-item">
								<a href="list_location.php?city=`+city_id+`&category=`+cat.category_id+`&city_name=`+city_name+`&category_name=`+cat.category_name+`" class="nav-link">
								  <i class="nav-icon fas fa-map"></i>
								  <p>
								    `+cat.category_name+`
								  </p>
								</a>
								</li>`;
							selector_city_sidebar.append(sidebar_city);
			            });

			       

	          },2000);
		     },
	        error: function(error){
	          console.log(error);
	        }
	      });
	  }


	
</script>

