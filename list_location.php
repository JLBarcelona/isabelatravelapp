<!DOCTYPE html>
<html>
<title>Main Page</title>
  <?php include("./Layout/header.php") ?>
  <!-- Header css meta -->
  <style type="text/css">
   .container-thumbnail {
    position: relative;
    width: 100%;
  }

  .image-thumb {
    display: block;
    width: 100%;
    height: auto;
  }

  .overlay {
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    background-color: rgba(5,5,5,0.5);
    overflow: hidden;
    width: 100%;
    height:100%;
    transition: .5s ease;
  }

  .container-thumbnail:hover .overlay {
    bottom: 0;
    /*height: 100%;*/
  }

  .text {
    color: white;
    text-shadow: 1px 1px #555;
    font-size: 20px;
    position: absolute;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
    text-align: center;
  }
  </style>
<body class="sidebar-mini layout-fixed ">
  <div class="wrapper">
  <!-- navbar -->
  <?php include("./Layout/nav.php") ?>
  <!-- Sidebar -->
  <?php include("./Layout/sidebar.php") ?>
   <section class="content-wrapper bg-white">
      <div class="container-fluid">
        <div class="row pt-3">
          <div class="col-sm-12 text-center mb-3 h4 title">
            <span class="bold"><?php echo $_REQUEST['city_name'] ?></span> <br> <small><?php echo $_REQUEST['category_name'] ?></small>
          </div>
        </div>
        <div class="row menus_filter_category_location mb-2">
        </div>
      </div>
    </section>
  </div>
</body>
<!-- sorting_disabled -->
  <!-- Footer Scripts -->
  <?php include("./Layout/footer.php") ?>

  <!-- <button onclick="locator('16.6830107,121.528243', '16.68569274633472,121.53985977172853');">Hey</button> -->

  <!-- Map 3rd party -->
  <!-- https://www.google.com/maps/dir/16.7197503,121.5678336/16.6733797,121.5530677/@16.6854656,121.552896,13z -->

<script type="text/javascript"> 
function show_modal_category(id){
    $("#"+id).modal({'backdrop' : 'static'});
  }


function show_locations(city_id, category_id){
    let urls = url+'?action=show_menus_category_location';
    $.ajax({
        type:"GET",
        url:urls,
        data:{city_id : city_id, category_id : category_id},
        dataType:'json',
        beforeSend:function(){
          let loader = ` <div class="col-sm-12 text-center col-12">
                        <img src="webroot/img/loading.gif" class="img-fluid animated fadeIn">
                      </div>`;
      $(".menus_filter_category_location").append(loader);     
        },
        success:function(response){
          console.log(response.locations);
          setTimeout(function(){
            let selector_location = $(".menus_filter_category_location");
            let selector_cat_sidebar = $(".cat_sidebar");
            let locations_category = response.locations;

            selector_location.html('');
            let loc = locations_category.map(loc => {
                let thumbnail = (loc.gallery.length > 0)? loc.gallery[0].uploaded_image : ''; 
                let output = ``;
                let id_modal = "modal_"+loc.category.location_id;
                let id_carosel = "carousel_"+loc.category.location_id;
                let i = 0;
                let ii = 0;
                let gallerys = loc.gallery;
                let destination = loc.category.lattitude+','+loc.category.longitude;

                $("#"+id_carosel).carousel();
                output += ` <div class="col-sm-6 col-6 mt-2"  onclick="show_modal_category(\'`+id_modal+`\')">
                                <div class="container-thumbnail">
                                  <img src="webroot/img/img.png" style="background-image:url(\'`+thumbnail+`\');" class="img-fluid animated fadeIn img-bg img-thumbnail image-thumb" style="width:100% !important;">
                                   <div class="overlay">
                                      <div class="text text-capitalize">`+loc.category.location_name+`</div>
                                    </div>
                                </div>
                              </div>`;


                output += `<div class="modal fade" role="dialog" id="`+id_modal+`">
                <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                <div class="modal-title text-capitalize h4 bold">
                `+loc.category.location_name+`
                </div>
                <button class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                <div>`+loc.category.description+`</div>
                <hr>
                <div id="`+id_carosel+`" class="carousel slide" data-ride="carousel">
                <ul class="carousel-indicators">`;


                output += gallerys.map(gal => {
                  let out ='';
                  if (i == 0) {
                   out += `<li data-target="#`+id_carosel+`" data-slide-to="`+i+`" class="active"></li>`;

                  }else{
                    out += `<li data-target="#`+id_carosel+`" data-slide-to="`+i+`"></li>`;
                  }
                  i++;
                  return out;
                });
                
                output += `</ul>
                <div class="carousel-inner">`;

                output += gallerys.map(gal => {
                  let out ='';

                  if (ii == 0) {
                    out += `<div class="carousel-item active">
                            <img src="`+gal.uploaded_image+`"class="img-fluid animated fadeIn img-thumbnail" style="width:100% !important;">
                            <div class="">
                            <p>`+gal.image_description+`</p>
                            </div>   
                            </div>`;
                    }else{
                       out += `<div class="carousel-item">
                            <img src="`+gal.uploaded_image+`"class="img-fluid animated fadeIn img-thumbnail" style="width:100% !important;">
                            <div class="">
                            <p>`+gal.image_description+`</p>
                            </div>   
                            </div>`;
                    }
                    ii++;

                    return out;
                  });


                  output += `</div>
                  <a class="carousel-control-prev" href="#`+id_carosel+`" data-slide="prev">
                  <span class="carousel-control-prev-icon"></span>
                  </a>
                  <a class="carousel-control-next" href="#`+id_carosel+`" data-slide="next">
                  <span class="carousel-control-next-icon"></span>
                  </a>
                  </div>
                  </div>
                  <div class="modal-footer">
                  <button class="btn btn-primary btn-block" onclick="locator(\'`+destination+`\');"><i class="fas fa-directions"></i> Get Directions</button>
                  </div>
                  </div>
                  </div>
                  </div>`;

              selector_location.append(output);
              i = 0;
            });

          },0);
       },
        error: function(error){
          console.log(error);
        }
      });
  }

  
  
  var lat = '16.6830107';
  var long = '121.528243';
  var origin = lat+','+long;

function getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
      // x.innerHTML = "Geolocation is not supported by this browser.";
      swal("Error",'Geolocation is not supported by this browser.', "error");
    }
}

function showPosition(position) {
   lat = position.coords.latitude;
   long = position.coords.longitude;
   origin = lat+' '+long;
  console.log(lat+' '+long);
}

function locator(destination){
  getLocation();
  // window.location =' https://www.google.com/maps/dir/?api=1&origin=34.1030032,-118.41046840000001&destination=34.059808,-118.368152';
  window.location =' https://www.google.com/maps/dir/?api=1&origin='+origin+'&destination='+destination;
}


</script>

 <script type="text/javascript">
    show_category_side('<?php echo $_REQUEST['city'] ?>','<?php echo $_REQUEST['city_name'] ?>');
    show_locations('<?php echo $_REQUEST['city'] ?>','<?php echo $_REQUEST['category'] ?>');
  </script>